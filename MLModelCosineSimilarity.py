# from operator import itemgetter
import os
from os import listdir
from os.path import isfile, join
from math import log, sqrt
from collections import defaultdict
import nltk

class CosineSimilarity(object):
    '''
    class docs
    '''
    
    # this function takes the dot product of the query with all the documents
    #  and returns a sorted list of tuples of docId, cosine score pairs
    def simpleCosineSimilarity(self , X_list):
        list_of_docs = []
        l1 =[];l2 =[] 
        Y_list = onlyfiles
        X_set = set(list(X_list))
        for file in Y_list:
            Y_set = set(list(file))
            rvector = set()
            rvector = X_set.union(Y_set)
            l1 = [];l2 = []
            for w in rvector: 
                if w in X_set: l1.append(1) # create a vector 
                else: l1.append(0) 
                if w in Y_set: l2.append(1) 
                else: l2.append(0)
            c = 0
            # cosine formula  
            for i in range(len(rvector)): 
                    c+= l1[i]*l2[i] 
            cosine = c / float((sum(l1)*sum(l2))**0.5)
            threshold = 0.70
            if cosine > float(threshold) : 
                list_of_docs.append(file)
        return list_of_docs

if __name__ == '__main__':

    cvs = CosineVectorSimilarity()
    cs = CosineSimilarity()
    X_list = input("Enter the File : ")
    files = cs.simpleCosineSimilarity(X_list)
    print(files)
