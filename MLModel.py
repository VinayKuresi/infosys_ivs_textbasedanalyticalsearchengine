import os
from os import listdir
from os.path import isfile, join
from math import log, sqrt
from collections import defaultdict
import nltk
from PIL import Image 
import pytesseract 
import sys 
import docx
import textract
from pdf2image import convert_from_path 

class CosineVectorSimilarity(object):
    '''
    class docs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self.inverted_index = defaultdict(list)
        self.BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        self.TEMPLATES_DIR = os.path.join(self.BASE_DIR , "infosysivstextbasedsearchengine\corpus")
        self.onlyfiles = [f for f in listdir(self.TEMPLATES_DIR) if isfile(join(self.TEMPLATES_DIR, f))]
        self.nos_of_documents = len(self.onlyfiles)
        self.vects_for_docs = []  
        self.document_freq_vect = {}  


    def iterate_over_all_docs(self):

        for file in self.onlyfiles:
            doc_text = self.get_document_text_from_doc_id(file)
            token_list = self.get_tokenized_and_normalized_list(doc_text)
            vect = self.create_vector(token_list)
            self.vects_for_docs.append(vect)

    def create_vector_from_query(self, l1):

        vect = {}
        for token in l1:
            if token in vect:
                vect[token] += 1.0
            else:
                vect[token] = 1.0
        return vect

    def generate_inverted_index(self):

        count1 = 0
        for vector in self.vects_for_docs:
            for word1 in vector:
                self.inverted_index[word1].append(count1)
            count1 += 1


    def create_tf_idf_vector(self):

        vect_length = 0.0
        for vect in self.vects_for_docs:
            for word1 in vect:
                word_freq = vect[word1]
                temp = self.calc_tf_idf(word1, word_freq)
                vect[word1] = temp
                vect_length += temp ** 2

            vect_length = sqrt(vect_length)
            for word1 in vect:
                vect[word1] /= vect_length


    def get_tf_idf_from_query_vect(self, query_vector1):

        vect_length = 0.0
        for word1 in query_vector1:
            word_freq = query_vector1[word1]
            if word1 in self.document_freq_vect:  
                query_vector1[word1] = self.calc_tf_idf(word1, word_freq)
            else:
                query_vector1[word1] = log(1 + word_freq) * log(
                    self.nos_of_documents)  

            vect_length += query_vector1[word1] ** 2
        vect_length = sqrt(vect_length)
        if vect_length != 0:
            for word1 in query_vector1:
                query_vector1[word1] /= vect_length


    def calc_tf_idf(self, word1, word_freq):

        return log(1 + word_freq) * log(self.nos_of_documents / self.document_freq_vect[word1])


    def get_dot_product(self, vector1, vector2):

        if len(vector1) > len(vector2): 
            temp = vector1
            vector1 = vector2
            vector2 = temp
        keys1 = vector1.keys()
        keys2 = vector2.keys()
        sum = 0
        for i in keys1:
            if i in keys2:
                sum += vector1[i] * vector2[i]
        return sum


    def get_tokenized_and_normalized_list(self, doc_text):

        tokens = nltk.word_tokenize(doc_text)
        ps = nltk.stem.PorterStemmer()
        stemmed = []
        for words in tokens:
            stemmed.append(ps.stem(words))
        return stemmed


    def create_vector(self, l1):

        vect = {}  

        for token in l1:
            if token in vect:
                vect[token] += 1
            else:
                vect[token] = 1
                if token in self.document_freq_vect:
                    self.document_freq_vect[token] += 1
                else:
                    self.document_freq_vect[token] = 1
        return vect


    def get_document_text_from_doc_id(self, file):

        try:
            str1 = open(self.TEMPLATES_DIR+ "\\" + file).read()
            str1 = str1 + str(file)
        except:
            str1 = ""
        return str1


    def get_result_from_query_vect(self, query_vector1):

        parsed_list = []
        for i , file in enumerate(self.onlyfiles):
            dot_prod = self.get_dot_product(query_vector1, self.vects_for_docs[i])
            parsed_list.append((str(file), dot_prod))
            parsed_list = sorted(parsed_list, key=lambda x: x[1])
        return parsed_list[-5:]

  
    def result(self , query):

        if len(query) == 0:
            return
        query_list = self.get_tokenized_and_normalized_list(query)
        query_vector = self.create_vector_from_query(query_list)
        self.get_tf_idf_from_query_vect(query_vector)
        result_set = self.get_result_from_query_vect(query_vector)

        # for tup in result_set[::-1]:
        #     print("The document is " + str(tup[0]) + " and the weight is " + str(tup[1]))
        # print()

        return result_set[::-1]

class Modelling(object):
    '''
    class docs
    '''

    def __init__(self):
        '''
        Constructor
        '''

    def setFile(self , finallist):
        
        ls = set()
        for fre in finallist:
            ls.add(fre[1])
        return ls , len(ls)

    def sortedList(self , ls , finallist):

        fileList = []
        for i in ls:
            dicvalue = []
            count = 0
            for j in finallist:
                if i == j[1]:
                    count += 1
            dicvalue.append(i)
            dicvalue.append(count)
            fileList.append(tuple(dicvalue))

        sortedList = sorted(fileList, key=lambda x: x[1])[::-1]

        return sortedList
    
    def fileCount(self , maxfile , sortedList):

        count = 0
        array = [[] for i in range(maxfile)]
        for j in sortedList : 
            if j[1] == maxfile :
                array[count].append(j)
            else : 
                maxfile -=  1
                count += 1
                array[count].append(j)

        return array

    def maxFiles(self, sortedList):

        maxfiles = sortedList[0][1]
        # maxfile = maxfiles
        return maxfiles
    
    def bookDictionary(self, finallist , maxfiles , array):

        finaldict = {}
        finaldict['finalfile'] = {}
        maxfile = maxfiles

        for i in array:
            for j in i:
                finaldict['finalfile'][j[0]] = {}
                totalValue = 0
                for k in finallist:
                    if j[0] == k[1]:
                        finaldict['finalfile'][j[0]][k[0]] = k[2]
                        totalValue += k[2]
                finaldict['finalfile'][j[0]]['totalScore'] = totalValue
                finaldict['finalfile'][j[0]]['AverageScore'] = totalValue/maxfile
            maxfile -= 1

        return finaldict

    def sortedBookDictionary(self , array , finaldict):
        
        finallist2 = []
        finalDict = {}
        finalDict['finalfile'] = {}
        for i in array:
            list2 = []
            for j in i:
                list1 = []
                list1.append(j[0])
                list1.append(finaldict['finalfile'][j[0]]['totalScore'])
                list2.append(tuple(list1))
            sortedList = sorted(list2, key=lambda x: x[1])[::-1]
            for i in sortedList:
                finalDict['finalfile'][i[0]] = finaldict['finalfile'][i[0]]
                finallist2.append(i[0])
        return finalDict , tuple(finallist2)

class TextExtraction(object):
    '''
    class docs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        pytesseract.pytesseract.tesseract_cmd = (
            r"C:\Program Files\Tesseract-OCR\tesseract.exe"
        )

    #Extract text from DOCX
    def getDocxContent(self , filename):
        doc = docx.Document(filename)
        fullText = ""
        for para in doc.paragraphs:
            fullText += para.text

        # Creating a text file to write the output 
        outfile = "corpus\out_text1.txt"

        f = open(outfile, "w") 
        # Finally, write the processed text to the file. 
        f.write(fullText)
        f.close()

    def getTextContext(self , filename):

        image_paragraphs = []
        text = textract.process(filename)
        text = text.decode("utf-8")
        outfile = "corpus\out_text2.txt"

        f = open(outfile, "w") 
        # Finally, write the processed text to the file. 
        f.write(fullText)
        f.close()
    
    def extract(self , PDF_file):

        # # Path of the pdf 
        # PDF_file = r"C:\Users\vinay\Downloads\headfirst.pdf"

        ''' 
        Part #1 : Converting PDF to images 
        '''

        # Store all the pages of the PDF in a variable 
        pages = convert_from_path(PDF_file) 

        # Counter to store images of each page of PDF to image 
        image_counter = 1

        # Iterate through all the pages stored above 
        for page in pages: 

            # Declaring filename for each page of PDF as JPG 
            # For each page, filename will be: 
            # PDF page 1 -> page_1.jpg 
            # PDF page 2 -> page_2.jpg 
            # PDF page 3 -> page_3.jpg 
            # .... 
            # PDF page n -> page_n.jpg 
            filename = "page_"+str(image_counter)+".jpg"
            
            # Save the image of the page in system 
            page.save(filename, 'JPEG') 

            # Increment the counter to update filename 
            image_counter = image_counter + 1

        ''' 
        Part #2 - Recognizing text from the images using OCR 
        '''
        # Variable to get count of total number of pages 
        filelimit = image_counter-1

        # Creating a text file to write the output 
        outfile = "corpus\out_text.txt"

        # Open the file in append mode so that 
        # All contents of all images are added to the same file 
        f = open(outfile, "a") 

        # Iterate from 1 to total number of pages 
        for i in range(1, filelimit + 1): 

            # Set filename to recognize text from 
            # Again, these files will be: 
            # page_1.jpg 
            # page_2.jpg 
            # .... 
            # page_n.jpg 
            filename = "page_"+str(i)+".jpg"
                
            # Recognize the text as string in image using pytesserct 
            text = str(((pytesseract.image_to_string(Image.open(filename))))) 

            # The recognized text is stored in variable text 
            # Any string processing may be applied on text 
            # Here, basic formatting has been done: 
            # In many PDFs, at line ending, if a word can't 
            # be written fully, a 'hyphen' is added. 
            # The rest of the word is written in the next line 
            # Eg: This is a sample text this word here GeeksF- 
            # orGeeks is half on first line, remaining on next. 
            # To remove this, we replace every '-\n' to ''. 
            text = text.replace('-\n', '')	 

            # Finally, write the processed text to the file. 
            f.write(text) 

        # Close the file after writing all the text. 
        f.close()


if __name__ == '__main__':

    cvs = CosineVectorSimilarity()

    cvs.iterate_over_all_docs()

    cvs.generate_inverted_index()

    cvs.create_tf_idf_vector()

    finallist = [] 
    queryList = []

    while True:
        query = input("Please enter the query1 ... ")
        if len(query) == 0:
            break
        queryList.append(query)
        result = cvs.result(query)
        threshold = 0.05
        for value in result : 
           valuedict = []
           if value[1] > float(threshold):
              valuedict.append(query)
              valuedict.append(value[0])
              valuedict.append(value[1])
              finallist.append(tuple(valuedict))

    listIndex = len(query)

    ml = Modelling()

    # call setLIst function, parameters finallist, return ls
    ls = ml.setFile(finallist)

    # call sortedList function parameters ls, finallist, return sortedList
    sortedList = ml.sortedList(ls , finallist)

    # call maxFiles parameters sortedList, return maxfile
    maxfile = ml.maxFiles(sortedList)
    
    # call fileCount array of file counts parameters maxfile , sortedList , return array
    array = ml.fileCount(maxfile , sortedList)

    # call bookDictionary parameters finallist, return finaldict
    finaldict = ml.bookDictionary(finallist , maxfile , array)
            
    # call sortedBookDictionary parameters finaldict, array, return finalDict
    finalDict = ml.sortedBookDictionary(array , finaldict)

    print(finalDict)