from tkinter import Tk, Button,Label,Scrollbar,Listbox,StringVar,Entry,W,E,N,S,END
from tkinter import ttk
from tkinter import messagebox
from tkinter import filedialog
import sqlite3
import pandas as pd
from sqlite3 import Error
from PIL import ImageTk,Image
from MLModel import CosineVectorSimilarity , Modelling , TextExtraction
# from MLModelCosineSimilarity import CosineSimilarity, CosineVectorSimilarity
# from sqlserver_connfig import dbconnfig
# import pypyodbc as pyo
import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = os.path.join(BASE_DIR , "infosysivstextbasedsearchengine")
print(BASE_DIR)
db_path = os.path.join(BASE_DIR, 'db.sqlite3')
conn = sqlite3.connect(db_path)


class Bookdb:
    '''
    class docs
    '''
    def __init__(self):
        BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        db_path = os.path.join(BASE_DIR, 'db.sqlite3')
        self.conn = sqlite3.connect(db_path)
        # self.conn = pyo.connect(**dbconnfig)
        self.cursor = conn.cursor()
        print("You have connected to the  database")
        print(conn)

    def __del__(self):
        self.conn.close()

    def view(self):
        self.cursor.execute("SELECT * FROM documents")
        rows = self.cursor.fetchall()
        return rows

    def insert(self, Book_Name , Book_Url):
        sql=("INSERT INTO documents(Book_Name , Book_Url)VALUES (?,?)")
        values =[Book_Name , Book_Url]
        self.cursor.execute(sql,values)
        self.conn.commit()
        messagebox.showinfo(title="Book Database",message="New book added to database")

    def update(self, ID, Book_Name , Book_Url):
        tsql = 'UPDATE documents SET  Book_Name = ?, Book_Url = ? WHERE ID=?'
        self.cursor.execute(tsql, [Book_Name , Book_Url , ID])
        self.conn.commit()
        messagebox.showinfo(title="Book Database",message="Book Updated")

    def delete(self, ID):
        delquery ='DELETE FROM documents WHERE ID = ?'
        self.cursor.execute(delquery, [ID])
        self.conn.commit()
        messagebox.showinfo(title="Book Database",message="Book Deleted")

db = Bookdb()

def get_selected_row(event):
    global selected_tuple
    index = list_bx.curselection()[0]
    selected_tuple = list_bx.get(index)
    title_entry.delete(0, 'end')
    title_entry.insert('end', selected_tuple[1])
    author_entry.delete(0, 'end')
    author_entry.insert('end', selected_tuple[2])
    # isbn_entry.delete(0, 'end')
    # isbn_entry.insert('end', selected_tuple[3])

def view_records():
    list_bx.delete(0, 'end')
    for row in db.view():
        list_bx.insert('end', row)

def add_book():
    db.insert(Book_Name.get(), Book_Url.get())
    list_bx.delete(0, 'end')
    list_bx.insert('end', (Book_Name.get(), Book_Url.get()))
    title_entry.delete(0, "end") # Clears input after inserting
    author_entry.delete(0, "end")
    # isbn_entry.delete(0, "end")
    conn.commit()

def MLModel():

    query = str(search_entry.get())
    if len(query) == 0:
        messagebox.showinfo("Message", "Query Length can't be zero.")
        search_entry.delete(0 , "end")
        search_entry1.delete(0 , "end")
        search_entry2.delete(0 , "end")
    else : 
        list_bx.delete(0, 'end')
        queryFinal = []
        queryList = [search_entry , search_entry1 , search_entry2]
        for i in queryList:
            if i.get() != "":
                queryFinal.append(i.get())
        # print(queryFinal)
        resultList = []
        finallist = []
        for i in queryFinal:
            result = cvs.result(i)
            # print(result)
            threshold = 0.05
            for value in result : 
                valuedict = []
                if value[1] > float(threshold):
                    valuedict.append(i)
                    valuedict.append(value[0])
                    valuedict.append(value[1])
                    finallist.append(tuple(valuedict))

        ml = Modelling()

        # call setLIst function, parameters finallist, return ls
        ls , count = ml.setFile(finallist)

        # call sortedList function parameters ls, finallist, return sortedList
        sortedList = ml.sortedList(ls , finallist)

        # call maxFiles parameters sortedList, return maxfile
        maxfile = ml.maxFiles(sortedList)
        
        # call fileCount array of file counts parameters maxfile , sortedList , return array
        array = ml.fileCount(maxfile , sortedList)

        # call bookDictionary parameters finallist, return finaldict
        finaldict = ml.bookDictionary(finallist , maxfile , array)
        # print(finaldict)
                
        # call sortedBookDictionary parameters finaldict, array, return finalDict
        finalDict , sortedTup = ml.sortedBookDictionary(array , finaldict)
        # print(finallist)

        listOfItems = pd.DataFrame(finalDict)
        # print(listOfItems)

        if finalDict is None:
            list_bx.insert('end', "No related documents Found in the DataBase")

        if finalDict:
            string1 = "Book Name             "
            if queryList[0].get() != "":
                string1 += "| Qy1tf_idfScore       "
            if queryList[1].get() != "":
                string1 += "| Qy2tf_idfScore         "
            if queryList[2].get() != "":
                string1 += "| Qy3tf_idfScore          "
            string1 += "| TotalScore      |    AverageScore"
            list_bx.insert('end' , string1)
            list_bx.insert('end' , "")

            # list_bx.insert('end', finalDict)

            for word in sortedTup:
                string1 = str(word)
                if queryList[0].get() != "":
                    try:
                        if finalDict['finalfile'][word][queryList[0].get()] != None:
                            string1 += " |   "+str(finalDict['finalfile'][word][queryList[0].get()])
                    except:
                        string1 += " |  0  "
                if queryList[1].get() != "":
                    try:
                        if finalDict['finalfile'][word][queryList[1].get()] != None:
                            string1 += " |   "+str(finalDict['finalfile'][word][queryList[1].get()])
                    except:
                        string1 += " |  0  "
                if queryList[2].get() != "":
                    try:
                        if finalDict['finalfile'][word][queryList[2].get()] != None:
                            string1 += " |   "+str(finalDict['finalfile'][word][queryList[2].get()])
                    except:
                        string1 += " |  0  "
                string1 += " |   "+str(finalDict['finalfile'][word]['totalScore'])
                string1 += " |   "+str(finalDict['finalfile'][word]['AverageScore'])
                list_bx.insert('end' , string1)
                list_bx.insert('end' , "")

            list_bx.insert('end', finalDict)
            search_entry.delete(0 , "end")
            search_entry1.delete(0 , "end")
            search_entry2.delete(0 , "end")

def delete_records():
    db.delete(selected_tuple[0])
    conn.commit()

def clear_screen():
    list_bx.delete(0,'end')
    title_entry.delete(0,'end')
    author_entry.delete(0,'end')
    # isbn_entry.delete(0,'end')

def update_records():
    # db.update(selected_tuple[0], title_text.get(), author_text.get(), isbn_text.get())
    title_entry.delete(0, "end") # Clears input after inserting
    author_entry.delete(0, "end")
    conn.commit()

def browsefunc():
    filename1 = filedialog.askopenfilename(filetypes=(("PDF files",
                                                        "*.xlxs;*.xls;*.*;*.xlsb;*.xlxm;*.xlsm;*.xltx;*.xltm;*.docx;*.dotx;*.docm;*.dotm;*.pptx;*.pptm;*.potx;*.potm"),
                                                        ("HTML files", "*.html;*.htm"),
                                                        ("All files", "*.*")))

    extension = filename1.split('.')[-1]
    # print(extension)
    txt = TextExtraction()
    
    if extension == 'pdf':
        messagebox.showinfo("ML training started", "Optical Character Recognization process Started.")
        txt.extract(filename1)
    else :
        messagebox.showinfo("Message", "Extraction Started.")
        txt.getTextContext(filename1)

def trainModel():
    messagebox.showinfo("Message" , "Training Started.")

    cvs.iterate_over_all_docs()

    cvs.generate_inverted_index()

    cvs.create_tf_idf_vector()

    messagebox.showinfo("Message" , "Training Finished.")

def on_closing():
    dd = db
    if messagebox.askokcancel("Quit", "Do you want to quit?"):
        root.destroy()
        del dd


root = Tk()  # Creates application window

root.title("My documents Database Application") # Adds a title to application window
root.configure(background="gray11")  # Add background color to application window
root.geometry("950x650")  # Sets a size for application window
root.resizable(width=False,height=False) # Prevents the application window from resizing

global cvs
cvs = CosineVectorSimilarity()

cvs.iterate_over_all_docs()

cvs.generate_inverted_index()

cvs.create_tf_idf_vector()

# Create Labels and entry widgets

title_label =ttk.Label(root,text="Document Name",background="turquoise1",font=("TkDefaultFont", 16))
title_label.grid(row=2, column=0, padx = 10, sticky=W)
Book_Name = StringVar()
title_entry = ttk.Entry(root,width=24,textvariable=Book_Name)
title_entry.grid(row=2, column=1, sticky=W)

author_label =ttk.Label(root,text="Document URL",background="turquoise1",font=("TkDefaultFont", 16))
author_label.grid(row=2, column=2, padx = 20, sticky=W)
Book_Url = StringVar()
author_entry = ttk.Entry(root,width=24,textvariable=Book_Url)
author_entry.grid(row=2, column=3,  sticky=W)

search_label =ttk.Label(root,text="Search Document",background="turquoise1",font=("TkDefaultFont", 16))
search_label.grid(row=4, column=0, padx = 20, sticky=W)

Book_Url = StringVar()
Book_name1 = StringVar()
Book_name2 = StringVar()
Book_name3 = StringVar()

search_entry = ttk.Entry(root,width=24,textvariable=Book_name1)
search_entry.grid(row=4, column=1,  sticky=W, padx = 20)
search_entry1 = ttk.Entry(root,width=24,textvariable=Book_name2)
search_entry1.grid(row=4, column=2,  sticky=W, padx = 20)
search_entry2 = ttk.Entry(root,width=24,textvariable=Book_name3)
search_entry2.grid(row=4, column=3,  sticky=W, padx = 20)


# Add a button to insert inputs into database

add_btn = Button(root, text="Add Document",bg="blue",fg="white",font="helvetica 10 bold",command=add_book)
add_btn.grid(row=0, column=4, sticky=W , pady = 20)

add_btn1 = Button(root, text="Search Document",bg="blue",fg="white",font="helvetica 10 bold",command=MLModel)
add_btn1.grid(row=4, column=4, sticky=W , pady = 20)

add_btn2 = Button(root, text="Upload Document",bg="blue",fg="white",font="helvetica 10 bold",command=browsefunc)
add_btn2.grid(row=5, column=1, sticky=W , padx = 10 , pady = 20)

add_btn3 = Button(root, text="Train Document",bg="blue",fg="white",font="helvetica 10 bold",command=trainModel)
add_btn3.grid(row=5, column=2, sticky=W , padx = 10 , pady = 20)

global list_bx

# Add  a listbox  to display data from database
list_bx = Listbox(root,height=13,width=35,font="helvetica 13",bg="light blue")
list_bx.grid(row=6,column=0, columnspan=25,sticky=W + E,pady=50,padx=15)
list_bx.bind('<<ListboxSelect>>',get_selected_row)

# Add scrollbar to enable scrolling
scroll_bar = Scrollbar(root)
scroll_bar.grid(row=1,column=8, rowspan=14,sticky=W )

list_bx.configure(yscrollcommand=scroll_bar.set) # Enables vetical scrolling
scroll_bar.configure(command=list_bx.yview)

view_btn = Button(root, text="View all documents",bg="blue",fg="white",font="helvetica 10 bold",command=view_records)
view_btn.grid(row=3, column=4 , sticky=W , pady = 20)#, sticky=tk.N)

root.mainloop()  # Runs the application until exit
